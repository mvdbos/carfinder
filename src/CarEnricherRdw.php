<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

use Doctrine\Common\Cache\Cache;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class CarEnricherRdw implements CarEnricher
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Cache
     */
    private $cache;

    public function __construct(Cache $cache, Client $client)
    {
        $this->cache = $cache;
        $this->client = $client;
    }

    public function enrich(Car $car)
    {
        if ($car->isRDWenriched === false) {

            $rdwInfo = $this->cache->fetch($car->licensePlate);

            if ($rdwInfo === false) {
                $this->client->setServerParameter('HTTP_USER_AGENT', 'Testing/' . uniqid());
                $crawler = $this->client->request(
                    'GET',
                    'https://ovi.rdw.nl/default.aspx?kenteken=' . $car->licensePlate
                );

                $rdwInfo = $crawler->html();
                $this->cache->save($car->licensePlate, $rdwInfo);
            }

            if (false !== stripos($rdwInfo, 'ErrorBlocked')
                || false !== stripos($rdwInfo, 'ontzegd')
            ) {
                $car->isRDWenriched = false;
                $this->cache->delete($car->licensePlate);

                $seconds = 2;
                echo "<br />- RDW access blocked, sleeping for $seconds seconds";
                flush();
                ob_flush();
                sleep($seconds);
                return;
            }

            $crawler = new Crawler(null, 'https://ovi.rdw.nl/default.aspx?kenteken=' . $car->licensePlate);
            $crawler->addHtmlContent($rdwInfo);

            try {
                $eigenarenString = $crawler->filter('#Eigenaren')->text();
                $eigenarenString = str_replace(' ', '', $eigenarenString);
                $eigenaren = explode('/', $eigenarenString);
                $car->privateOwnerCount = (int) $eigenaren[0];
                $car->businessOwnerCount = (int) $eigenaren[1];

                $eersteAfgifteNederland = \DateTime::createFromFormat(
                    'd-m-Y',
                    $crawler->filter('#EersteAfgifteNederland')->text()
                );
                $eersteToelatingsdatum = \DateTime::createFromFormat(
                    'd-m-Y',
                    $crawler->filter('#EersteToelatingsdatum')->text()
                );

                if ($eersteAfgifteNederland && $eersteToelatingsdatum) {
                    $car->isImport = $eersteAfgifteNederland->diff($eersteToelatingsdatum)->y > 1;
                }

                $variant = $crawler->filter('#Variant')->text();
                $car->edition = $car->edition . " $variant";
                $car->isRDWenriched = true;
            } catch (\InvalidArgumentException $e) {
                // ignore when info is not available
                echo "\n- Ignored RDW error: $car->makeAndModel $car->edition :: $car->licensePlate " .
                    $e->getMessage() . $e->getTraceAsString();
            }
        }
    }
}
