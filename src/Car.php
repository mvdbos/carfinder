<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

class Car
{
    public $price;
    public $currencyName;
    public $licensePlate;
    public $ownerType;
    public $makeAndModel;
    public $edition;
    public $uri;
    public $isImport;
    public $ownerCount;
    public $privateOwnerCount;
    public $businessOwnerCount;
    public $isRDWenriched = false;
    public $buildYear;
    public $mileage;
    public $roundedMileage;
    public $roundedPrice;
    public $isForeign = false;

    /**
     * Liters / 100KM
     * @var double
     */
    public $fuelEconomy;

    /**
     * @param string $uri
     */
    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return float
     */
    public function getScore()
    {
        $yearScore = -1;
        if ($this->buildYear) {
            $yearScore = (7 - (date('Y') - $this->buildYear)) * 2;
        }

        $mileageScore = -1;
        if ($this->roundedMileage) {
            $mileageRatio = (120 - $this->roundedMileage);
            $mileageRatio = $mileageRatio / 10;
            $mileageScore = $mileageRatio * 1.5; // 1.5 pts per 10k km
        }

        $priceScore = -1;
        if ($this->price) {
            $adjustedPrice = $this->price;
            if ($this->isForeign) {
                $adjustedPrice = $adjustedPrice + 2500;
            }

            $priceDifference = (6000 - $adjustedPrice);

            $priceRatio = $priceDifference / 500;
            $priceScore = $priceRatio * 3; // 3 pts per 500 EUR away from 5000 EUR

        }

          $fuelEconomyScore = 0;

        return (float) round(10 + $yearScore + $mileageScore + $priceScore + $fuelEconomyScore);
    }
}
