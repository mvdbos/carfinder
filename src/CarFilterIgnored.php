<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

class CarFilterIgnored extends CarFilterAbstract
{
    private $ignored = [];

    /**
     * @param string[] $ignored
     */
    public function __construct(array $ignored)
    {
        $this->ignored = $ignored;
    }

    /**
     * This function returns true when the Car passed to it should be filtered
     *
     * @param Car $car
     *
     * @return bool returns true if this filter matches the car.
     */
    public function filter(Car $car)
    {
        if (in_array($car->licensePlate, $this->ignored)) {
            $this->filteredCount++;

            return true;
        }

        return false;
    }
}
