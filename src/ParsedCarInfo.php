<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

class ParsedCarInfo
{
    public $price;
    public $currencyName;
    public $licensePlate;
    public $ownerType;
    public $makeAndModel;
    public $edition;
    public $isImport;
    public $ownerCount;
    public $privateOwnerCount;
    public $businessOwnerCount;
    public $isRDWenriched = false;
    public $buildYear;
    public $mileage;
    public $roundedMileage;
    public $roundedPrice;
    public $isForeign = false;

    /**
     * Liters / 100KM
     * @var double
     */
    public $fuelEconomy;
}
