<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

interface CarFilter
{
    /**
     * @return integer The number of cars this filter has filtered
     */
    public function getFilteredCount();

    /**
     * This function returns true when the Car passed to it should be filtered
     *
     * @param Car $car
     *
     * @return bool returns true if this filter matches the car.
     */
    public function filter(Car $car);
}
