<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

/**
 * Interface CarFinderParser
 *
 * Knows how to parse the DOM of a specific car search site
 *
 * @package VDB
 */
interface CarFinderParser
{

    /**
     * @param string $html
     *
     * @return string|null the href of the next page
     */
    public function findNextPageHref($html);

    /**
     * @param string $html
     *
     * @return string[] A list of URIs that point to car detail pages
     */
    public function parseResultsList($html);

    /**
     * @param string $html
     *
     * @return ParsedCarInfo
     */
    public function parseCarPage($html);
}
