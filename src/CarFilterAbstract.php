<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

abstract class CarFilterAbstract implements CarFilter
{
    protected $filteredCount = 0;
    /**
     * @return integer The number of cars this filter has filtered
     */
    public function getFilteredCount()
    {
        return $this->filteredCount;
    }
}
