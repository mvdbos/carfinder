<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

interface CarEnricher
{
    /**
     * @return void
     */
    public function enrich(Car $car);
}
