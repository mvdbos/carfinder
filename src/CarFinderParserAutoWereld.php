<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

use NumberFormatter;
use Symfony\Component\DomCrawler\Crawler;

class CarFinderParserAutoWereld implements CarFinderParser
{
    public function findNextPageHref(Crawler $crawler)
    {
        try {
            return $crawler->filter('div.paginator a.volgende')->attr('href');
        } catch (\InvalidArgumentException $e) {
            return null;
        }
    }
    /**
     * @param Crawler $crawler
     *
     * @return Crawler A new instance of Crawler with the filtered list of nodes containing the the anchors to the detail pages
     */
    public function parseResultsList(Crawler $crawler)
    {
        return $crawler->filter('#content-inhoud > div > div.resultaatgrid td.omschrijving > h3 > a');
    }

    /**
     * @param Car $car
     * @param Crawler $crawler
     *
     * @return Car
     */
    public function parseCarPage(Car &$car, Crawler $crawler)
    {
        try {
            $priceString = trim($crawler->filter('div.occasiondetail  div.info > span.prijs > strong > span')->text());
            $car->price = preg_replace("/[^0-9]/", "", $priceString);

            $car->roundedPrice = round($car->price / 1000, 1);

            $mileageString = trim(
                $crawler->filter('#content-inhoud > div > div.occasiondetail div.hoofdspecs > dl > dd:nth-child(4)')->text()
            );

            $formatter = NumberFormatter::create('nl_NL', NumberFormatter::DECIMAL);
            $car->mileage = $formatter->parse($mileageString, NumberFormatter::TYPE_INT32);

            $car->roundedMileage = round($car->mileage / 10000) * 10;

            $fuelEconomyString = trim(
                $crawler->filter('#meerspecs > dl:nth-child(5) > dd:nth-child(6)')->text()
            );

            $fuelEconomyInt = (int) substr($fuelEconomyString, -2);
            if ($fuelEconomyInt) {
                $car->fuelEconomy = (float) 100 / $fuelEconomyInt;
            }

            $car->buildYear = trim(
                $crawler->filter('#content-inhoud > div > div.occasiondetail div.hoofdspecs > dl > dd:nth-child(2)')->text()
            );

            $car->buildYear = substr($car->buildYear, -4);

            $car->licensePlate = trim(
                $crawler->filter('#meerspecs > dl:nth-child(2) > dd:nth-child(6)')->text()
            );

            $car->ownerType = null;

            $car->makeAndModel = trim(
                $crawler->filter('#content-inhoud > div > div.occasiondetail > div.header > div.grid-12.clearfix > h1 > span:nth-child(1)')->text()
            );
            $car->edition = trim(
                $crawler->filter('#content-inhoud > div > div.occasiondetail > div.header > div.grid-12.clearfix > h1 > span:nth-child(2)')->text()
            );
            $car->ownerCount = null;

            $car->isImport = null;

        } catch (\InvalidArgumentException $e) {
            // ignore when info is not available
            echo "\n- Ignored Autowereld error: " . $car->makeAndModel . ' '. $car->edition . ' ' . $e->getMessage();
        }

        return $car;
    }
}
