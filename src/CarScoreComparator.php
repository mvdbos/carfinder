<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

use PHPExtra\Sorter\Comparator\ComparatorInterface;

class CarScoreComparator implements ComparatorInterface
{
    /**
     * Compare a to b
     * Returns -1 if a < b, 0 if a = b and 1 if a > b
     *
     * @param float|integer $prev
     * @param float|integer $next
     *
     * @return int
     */
    public function compare($prev, $next)
    {
        if ($prev === $next) {
            return 0;
        }

        return ($prev < $next) ? -1 : 1;
    }

    /**
     * Tell if current ComparatorInterface supports given value
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function supports($value)
    {
        return is_numeric($value);
    }
}
