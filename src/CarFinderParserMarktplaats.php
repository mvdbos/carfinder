<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

use NumberFormatter;
use Symfony\Component\DomCrawler\Crawler;

class CarFinderParserMarktplaats implements CarFinderParser
{
    public function findNextPageHref($html)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($html);

        try {
            return $crawler->filter('#pagination a.pagination-next')->attr('href');
        } catch (\InvalidArgumentException $e) {
            return null;
        }
    }

    /**
     * @param string $html
     *
     * @return string[] A list of URIs that point to car detail pages
     */
    public function parseResultsList($html)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($html);

        $uris = [];

        $crawler->filter('tr.defaultSnippet > td.column-listing > div.listing-title-description > h2 > a')->each(
            function ($node) use (&$uris) {
                /** @var Crawler $node */
                $uris[] = $node->attr('href');
            }
        );

        return $uris;
    }

    /**
     * @param string $html
     *
     * @return ParsedCarInfo
     */
    public function parseCarPage($html)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($html);

        $car = new ParsedCarInfo();

        try {
            $priceString = trim($crawler->filter('#vip-ad-price-container > span')->text());
            // replace regular spaces with nonbreaking spaces a required by ICU
            $priceString = str_replace("\x20", "\xC2\xA0", $priceString);

            $fmt = new NumberFormatter('nl_NL', NumberFormatter::CURRENCY);
            $car->price = $fmt->parseCurrency($priceString, $car->currencyName);

            $car->roundedPrice = round($car->price / 1000, 1);

            if (intl_is_failure($fmt->getErrorCode())) {
                echo "\nFormatter error: " . $fmt->getErrorMessage();
            }

            $mileageString = trim(
                $crawler->filter('#usps-block-container > div:nth-child(1) > div.usp-block-value')->text()
            );

            $formatter = NumberFormatter::create('nl_NL', NumberFormatter::DECIMAL);
            $car->mileage = $formatter->parse($mileageString, NumberFormatter::TYPE_INT32);

            $car->roundedMileage = round($car->mileage / 10000) * 10;

            $fuelEconomyString = trim(
                $crawler->filter('#usps-block-container > div:nth-child(3) > div.usp-block-value')->text()
            );

            $fuelEconomyDouble = $formatter->parse($fuelEconomyString, NumberFormatter::TYPE_DOUBLE);
            if ($fuelEconomyDouble) {
                $car->fuelEconomy = 100 / (float) $fuelEconomyDouble;
            }

            $car->buildYear = trim(
                $crawler->filter('#usps-block-container > div:nth-child(2) > div.usp-block-value')->text()
            );
            $car->licensePlate = trim(
                $crawler->filter('#car-features-general > tbody > tr:nth-child(4) > td:nth-child(2)')->text()
            );
            $car->ownerType = trim(
                $crawler->filter('#car-features-history > tbody > tr:nth-child(4) > td:nth-child(2)')->text()
            );
            $car->makeAndModel = trim(
                $crawler->filter('#car-features-general > tbody > tr:nth-child(1) > td:nth-child(2)')->text()
            );
            $car->edition = trim(
                $crawler->filter('#car-features-general > tbody > tr:nth-child(2) > td:nth-child(2)')->text()
            );
            $car->ownerCount = (int) trim(
                $crawler->filter('#car-features-history > tbody > tr:nth-child(2) > td:nth-child(2)')->text()
            );
            $car->isImport = trim(
                $crawler->filter(
                    '#car-features-history > tbody > tr:nth-child(6) > td:nth-child(2)'
                )->text()
            ) === 'Ja';
        } catch (\InvalidArgumentException $e) {
            // ignore when info is not available
            echo "\n- Ignored Marktplaats error: " . $e->getMessage();
        }

        return $car;
    }
}
