<?php
namespace VDB;

use Doctrine\Common\Cache\Cache;
use Goutte\Client;

class CarFinder
{
    /**
     * @var CarFilter[]
     */
    private $preEnrichmentFilters;

    /**
     * @var CarFilter[]
     */
    private $postEnrichmentFilters;

    /**
     * @var CarEnricher[]
     */
    private $enrichers;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Cache
     */
    private $cache;

    public function __construct(Cache $cache, Client $client)
    {
        $this->cache = $cache;
        $this->client = $client;

        $this->preEnrichmentFilters = [];
        $this->postEnrichmentFilters = [];
        $this->enrichers = [];

        // This filter is hardcoded intentionally: cars without license plates are useless.
        $this->registerPreEnrichmentFilter(new CarFilterNoLicensePlate());

        // This filter is hardcoded intentionally: duplicate Cars are useless.
        $this->registerPreEnrichmentFilter(new CarFilterDuplicate());
    }

    public function registerPreEnrichmentFilter(CarFilter $filter)
    {
        $this->preEnrichmentFilters[] = $filter;
    }

    public function registerPostEnrichmentFilter(CarFilter $filter)
    {
        $this->postEnrichmentFilters[] = $filter;
    }

    public function registerEnrichers(CarEnricher $enricher)
    {
        $this->enrichers[] = $enricher;
    }

    /**
     * @param string $uri
     * @param CarFinderParser $carFinderParser
     *
     * @return Car[]
     */
    public function find($uri, CarFinderParser $carFinderParser)
    {
        $searchResultHtml = $this->getSearchResultHtml($uri);

        $cars = $this->getCars($carFinderParser, $searchResultHtml);

        // if there is a next page, recurse
        $href = $carFinderParser->findNextPageHref($searchResultHtml);
        if ($href) {
            $href = $this->forceAbsoluteHref($uri, $href);

            $cars = array_merge($cars, $this->find($href, $carFinderParser));
        }

        return $cars;
    }

    /**
     * @param string $uri
     * @param CarFinderParser $carFinderParser
     *
     * @return Car|null
     */
    private function getCar($uri, CarFinderParser $carFinderParser)
    {
        $carHtml = $this->getCarHtml($uri);

        $parsedCarInfo = $carFinderParser->parseCarPage($carHtml);

        $car = $this->buildCarFromCarInfo($uri, $parsedCarInfo);

        foreach ($this->preEnrichmentFilters as $filter) {
            if ($filter->filter($car)) {
                return null; // no more filtering, skip this car entirely
            }
        }

        foreach ($this->enrichers as $enricher) {
            $enricher->enrich($car);
        }

        foreach ($this->postEnrichmentFilters as $filter) {
            if ($filter->filter($car)) {
                return null; // no more filtering, skip this car entirely
            }
        }

        return $car;
    }

    /**
     * @param string $uri
     *
     * @return string
     */
    private function getCarHtml($uri)
    {
        $carHtml = $this->cache->fetch($uri);

        if ($carHtml === false) {
            $crawler = $this->client->request(
                'GET',
                $uri
            );

            $carHtml = $crawler->html();

            $this->cache->save($uri, $carHtml);
        }

        return $carHtml;
    }

    /**
     * @param string $uri
     *
     * @return string The html of the searchresult
     */
    private function getSearchResultHtml($uri)
    {
        $cacheKey = md5($uri);
        $searchResult = $this->cache->fetch($cacheKey);

        if ($searchResult === false) {

            $this->client->setServerParameter('HTTP_USER_AGENT', 'Testing/' . uniqid());

            $crawler = $this->client->request(
                'GET',
                $uri
            );

            $searchResult = $crawler->html();
            $cacheLifeTime = 60 * 60; // one hour
            $this->cache->save($cacheKey, $searchResult, $cacheLifeTime);

            return $searchResult;
        }

        return $searchResult;
    }

    /**
     * @param string $uri
     * @param ParsedCarInfo $parsedCarInfo
     *
     * @return Car
     */
    private function buildCarFromCarInfo($uri, $parsedCarInfo)
    {
        // hack for now, clean this mapping up later
        $car = new Car($uri);
        foreach (get_object_vars($parsedCarInfo) as $property => $value) {
            $car->$property = $value;
        }

        return $car;
    }

    /**
     * @param string $baseUri
     * @param string $href
     *
     * @return string
     */
    private function forceAbsoluteHref($baseUri, $href)
    {
        // if the uri is relative, make it absolute
        if (parse_url($href, PHP_URL_HOST) === null) {
            $originalHost = parse_url($baseUri, PHP_URL_HOST);
            $originalScheme = parse_url($baseUri, PHP_URL_SCHEME);
            $href = $originalScheme . '://' . $originalHost . $href;

            return $href;
        }

        return $href;
    }

    /**
     * @param CarFinderParser $carFinderParser
     * @param string $searchResultHtml
     *
     * @return Car[]
     */
    private function getCars(CarFinderParser $carFinderParser, $searchResultHtml)
    {
        $uris = $carFinderParser->parseResultsList($searchResultHtml);

        $cars = [];
        foreach ($uris as $uri) {
            $car = $this->getCar($uri, $carFinderParser);

            if ($car === null) {
                continue;
            }

            $cars[$car->licensePlate] = $car;
        }

        return $cars;
    }
}
