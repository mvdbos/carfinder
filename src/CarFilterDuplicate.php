<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

class CarFilterDuplicate extends CarFilterAbstract
{
    /**
     * @var array All license plates this filter encountered
     */
    private $processedLicensePlates = [];

    /**
     * This function returns true when the Car passed to it should be filtered
     *
     * @param Car $car
     *
     * @return bool returns true if this filter matches the car.
     */
    public function filter(Car $car)
    {
        if (in_array($car->licensePlate, $this->processedLicensePlates)) {
            $this->filteredCount++;

            return true;
        }

        $this->processedLicensePlates[] = $car->licensePlate;

        return false;
    }
}
