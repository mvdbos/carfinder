<?php
/**
 * @author Matthijs van den Bos <matthijs@vandenbos.org>
 * @copyright 2014 Matthijs van den Bos
 */

namespace VDB;

use Symfony\Component\DomCrawler\Crawler;

class CarFinderParserMobileDe implements CarFinderParser
{
    public function findNextPageHref(Crawler $crawler)
    {
        try {
            return $crawler->filter('a.page-next')->attr('href');
        } catch (\InvalidArgumentException $e) {
            return null;
        }
    }
    /**
     * @param Crawler $crawler
     *
     * @return Crawler A new instance of Crawler with the filtered list of nodes containing the the anchors to the detail pages
     */
    public function parseResultsList(Crawler $crawler)
    {
        return $crawler->filter('div.vehicleDetails.vehicleDetailsMain > div.listEntryTitle > a.detailsViewLink');
    }

    /**
     * @param Car $car
     * @param Crawler $crawler
     *
     * @return Car|null
     */
    public function parseCarPage(Car &$car, Crawler $crawler)
    {
        try {
            $vendorZipCode = trim($crawler->filterXPath('//*[@id="mainVendorDetailsAddress"]/text()[2]')->text());
            $vendorZipCode = preg_replace("/[^0-9]/", "", $vendorZipCode);
            $vendorZipCodeArea = substr($vendorZipCode, 0, 1);
            $allowedZipAreas = [2, 4, 5, 6];

            if (in_array($vendorZipCodeArea, $allowedZipAreas) === false) {
                return null;
            }

            $priceString = trim($crawler->filter('div.vehicleDetails > p.pricePrimaryCountryOfSale.priceGross')->text());
            $car->price = preg_replace("/[^0-9]/", "", $priceString);

            $car->roundedPrice = round($car->price / 1000, 1);

            $mainTechnicalDataOffset = $crawler->filter('div.vehicleDetails > div.mainTechnicalData')->children()->count() - 7;

            $mileageOffset = 4 + $mainTechnicalDataOffset;
            $mileageString = trim(
                $crawler->filter('div.vehicleDetails > div.mainTechnicalData > p:nth-child('. $mileageOffset.')')->text()
            );

            $car->mileage = preg_replace("/[^0-9]/", "", $mileageString);
            $car->roundedMileage = round($car->mileage / 10000) * 10;

            $buildYearOffset = 3 + $mainTechnicalDataOffset;
            $car->buildYear = trim(
                $crawler->filter('div.vehicleDetails > div.mainTechnicalData > p:nth-child('.$buildYearOffset.')')->text()
            );

            $car->buildYear = substr($car->buildYear, -4);

            $car->licensePlate = 'foreign-' . uniqid();

            $car->ownerType = null;

            $car->makeAndModel = trim(
                $crawler->filter('section.vehicleTitle > div.titleContainer > h1')->text()
            );
            $car->edition = '';

            $car->ownerCount = 1;

            $car->isImport = null;
            $car->isForeign = true;

            $car->isRDWenriched = true;

        } catch (\InvalidArgumentException $e) {
            // ignore when info is not available
            echo "\n- Ignored Mobile.de error: " . $car->makeAndModel . ' '. $car->edition . ': ' . $e->getMessage() . $e->getTraceAsString();
            flush();
            ob_flush();
        }

        return $car;
    }
}
