<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once "bootstrap.php";

use PHPExtra\Sorter\Sorter;
use PHPExtra\Sorter\Strategy\ObjectSortStrategy;

$sessionId = $_GET['id'];

/** @var \Doctrine\Common\Cache\Cache $searchResultCache */
$searchResultCache = $container['cache.searchresult'];

$searchResultFound = $searchResultCache->contains($sessionId);

if ($searchResultFound === false) {
    header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
    echo "Still Searching...";
    echo '<script>window.setTimeout(function(){location.reload()},3000)</script>';
    exit();
}

$cars = $searchResultCache->fetch($sessionId);

$totalCars = count($cars);
$selected = 0;
$noLicensePlate = 0;
$imported = 0;
$tooManyOwners = 0;
$wrongBrand = 0;
$duplicates = 0;
$ignored = 0;
$processedLicensePlates = [];

// sort
$strategy = new ObjectSortStrategy();
$strategy
    ->sortBy(
        function ($object) {
            return $object->getScore();
        },
        Sorter::DESC,
        new \VDB\CarScoreComparator()
    )
    ->sortBy('makeAndModel', Sorter::ASC)
    ->sortBy('edition', Sorter::ASC)
    ->sortBy('buildYear', Sorter::DESC)
    ->sortBy('roundedMileage', Sorter::ASC);

$sorter = new Sorter();
$sortedCars = $sorter->setStrategy($strategy)->sort($cars);

// render
/** @var Twig_Environment $twig */
$twig = $container['twig'];

echo "<h2>Selected: " . count($sortedCars) . " / $totalCars</h2>";
$template = $twig->loadTemplate('index.twig');

echo $template->render(array('cars' => $sortedCars));

echo "<h2>Excluded: " . ($totalCars - count($sortedCars)) . " / $totalCars</h2>";
echo "<ul>";
echo "<li>Imported: $imported</li>";
echo "<li>Too many owners: $tooManyOwners</li>";
echo "<li>No License plate: $noLicensePlate</li>";
echo "<li>Wrong brand: $wrongBrand</li>";
echo "<li>Duplicates: $duplicates</li>";
echo "<li>Ignored: $ignored</li>";
echo "</ul>";
