<?php
$container = new Pimple\Container();

$container['filter.nolicenseplate'] = function () {
    return new \VDB\CarFilterNoLicensePlate();
};

$container['filter.ignored'] = function ($c) {
    return new \VDB\CarFilterIgnored($c['data.ignored']);
};

$container['filter.duplicate'] = function () {
    return new \VDB\CarFilterDuplicate();
};

$container['filter.ownercount.preenrichment'] = function () {
    return new \VDB\CarFilterOwnerCountPreEnrichment();
};

$container['filter.ownercount.postenrichment'] = function () {
    return new \VDB\CarFilterOwnerCountPostEnrichment();
};

$container['filter.isimport'] = function () {
    return new \VDB\CarFilterIsImport();
};

$container['amqp.channel'] = function ($c) {
    /** @var \PhpAmqpLib\Channel\AMQPChannel $channel */
    $channel = $c['amqp.connection']->channel();

    $channel->basic_qos(null, 1, false);

    $channel->queue_declare('task_queue', false, true, false, false);

    return $channel;
};

$container['amqp.connection'] = function () {
    $url = parse_url(getenv('CLOUDAMQP_URL'));
    $vhost = substr($url['path'], 1);
    if (strlen($vhost) === 0) {
        $vhost = '/';
    }
    $connection = new \PhpAmqpLib\Connection\AMQPConnection(
        $url['host'],
        5672,
        $url['user'],
        $url['pass'],
        $vhost
    );

    return $connection;
};

$container['enricher.rdw'] = function ($c) {
    return new \VDB\CarEnricherRdw($c['cache.rdw'], $c['crawler']);
};

$container['carfinder'] = function ($c) {
    $carFinder = new \VDB\CarFinder($c['cache.sitesearch'], $c['crawler']);

    return $carFinder;
};

$container['data.ignored'] = function () {
    return file(__DIR__ . DIRECTORY_SEPARATOR . 'data/ignored', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
};

$container['twig'] = function () {
    $loader = new Twig_Loader_Filesystem(__DIR__ . DIRECTORY_SEPARATOR . 'templates');
    $twig = new Twig_Environment($loader, array(
        'debug' => true,
        'cache' => __DIR__ . DIRECTORY_SEPARATOR . 'cache/twig'
    ));

    return $twig;
};

$container['mailer'] = function () {
    $transport = Swift_SmtpTransport::newInstance(getenv('POSTMARK_SMTP_SERVER'), 25)
        ->setPassword(getenv('POSTMARK_API_KEY'))
        ->setUsername(getenv('POSTMARK_API_KEY'))
        ->setAuthMode('cram-md5')
        ->setEncryption('tls')
        ->setLocalDomain('warm-meadow-2406.herokuapp.com');
    $mailer = Swift_Mailer::newInstance($transport);

    return $mailer;
};
$container['crawler'] = function () {
    $client = new \Goutte\Client();
    $client->getClient()->setDefaultOption('config/curl/' . CURLOPT_TIMEOUT, 60);
    $client->getClient()->setDefaultOption('config/curl/' . CURLOPT_SSLVERSION, 3);

    return $client;
};

$container['cache.searchresult'] = function ($c) {
    $cache = new \Doctrine\Common\Cache\MemcachedCache();
    $cache->setMemcached($c['memcached']);
    $cache->setNamespace('searchresult');

    return $cache;
};

$container['cache.sitesearch'] = function ($c) {
    $cache = new \Doctrine\Common\Cache\MemcachedCache();
    $cache->setMemcached($c['memcached']);
    $cache->setNamespace('sitesearch');

    return $cache;
};

$container['cache.carpage'] = function ($c) {
    $cache = new \Doctrine\Common\Cache\MemcachedCache();
    $cache->setMemcached($c['memcached']);
    $cache->setNamespace('carpage');

    return $cache;
};

$container['cache.rdw'] = function ($c) {
    $cache = new \Doctrine\Common\Cache\MemcachedCache();
    $cache->setMemcached($c['memcached']);
    $cache->setNamespace('rdw');

    return $cache;
};

$container['memcached'] = function () {
    // create a new persistent client
    $m = new Memcached("memcached_pool");
    $m->setOption(Memcached::OPT_BINARY_PROTOCOL, true);

    // setup authentication
    if (getenv("MEMCACHIER_USERNAME") && getenv("MEMCACHIER_PASSWORD")) {
        $m->setSaslAuthData(
            getenv("MEMCACHIER_USERNAME"),
            getenv("MEMCACHIER_PASSWORD")
        );
    }

    // We use a consistent connection to memcached, so only add in the
    // servers first time through otherwise we end up duplicating our
    // connections to the server.
    if (count($m->getServerList()) === 0) {
        // parse server config
        $servers = explode(",", getenv("MEMCACHIER_SERVERS"));
        foreach ($servers as $s) {
            $parts = explode(":", $s);
            $m->addServer($parts[0], $parts[1]);
        }
    }

    return $m;
};
