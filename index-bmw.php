<?php
require_once "vendor/autoload.php";
require_once "bootstrap.php";

session_start();
$sessionId = session_id();

/** @var \Doctrine\Common\Cache\Cache $searchResultCache */
$searchResultCache = $container['cache.searchresult'];

if ($searchResultCache->contains($sessionId)) {
    $newUrl = "/result.php?id=$sessionId";
    header('Location: ' . $newUrl);
    exit();
}

// make an initial search results entry, valid for 60 secs
$searchResultCache->save('search-in-progress-' . $sessionId, true, 120);

/** @var \PhpAmqpLib\Channel\AMQPChannel $channel */
$channel = $container['amqp.channel'];

$data = json_encode([
    'sessionId' => $sessionId,
    'uri' => 'http://www.marktplaats.nl/z/auto-s/bmw/benzine-stationwagon-1-4-2-0-liter-2-1-3-0-liter.html?categoryId=96&searchOnTitleAndDescription=true&attributes=S%2C10882&priceFrom=3.000%2C00&priceTo=8.500%2C00&yearFrom=2003&yearTo=&mileageFrom=&mileageTo=200.000&attributes=S%2C610&attributes=S%2C473&attributes=S%2C484&attributes=N%2C73&attributes=N%2C74&startDateFrom=always',
    'parserClass' => '\VDB\CarFinderParserMarktplaats',
    'preEnrichmentFilters' => [
        'filter.ignored',
        'filter.ownercount.preenrichment',
    ],
    'postEnrichmentFilters' => [
        'filter.ownercount.postenrichment',
    ],
    'enrichers' => [
        'enricher.rdw'
    ]
]);

$msg = new \PhpAmqpLib\Message\AMQPMessage(
    $data,
    array('delivery_mode' => 2) // make message persistent
);

$channel->basic_publish($msg, '', 'task_queue');

$newUrl = "/result.php?id=$sessionId";
header('Location: ' . $newUrl);
