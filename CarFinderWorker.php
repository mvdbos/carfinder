<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once "bootstrap.php";

/** @var \PhpAmqpLib\Connection\AMQPConnection $connection */
$connection = $container['amqp.connection'];

/** @var \PhpAmqpLib\Channel\AMQPChannel $channel */
$channel = $container['amqp.channel'];

/** @var \Doctrine\Common\Cache\Cache $searchResultCache */
$searchResultCache = $container['cache.searchresult'];

/** @var \VDB\CarFinder $carFinder */
$carFinder = $container['carfinder'];

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function ($msg) use ($container, $searchResultCache, $carFinder) {
    echo " [x] Received ", $msg->body, "\n";

    $decodedMsg = json_decode($msg->body);
    $sessionId = $decodedMsg->sessionId;
    $uri = $decodedMsg->uri;
    $parserClass = $decodedMsg->parserClass;
    $preEnrichmentFilters = $decodedMsg->preEnrichmentFilters;
    $postEnrichmentFilters = $decodedMsg->postEnrichmentFilters;
    $enrichers = $decodedMsg->enrichers;

    foreach ($preEnrichmentFilters as $filterName) {
        $carFinder->registerPreEnrichmentFilter($container[$filterName]);
    }

    foreach ($postEnrichmentFilters as $filterName) {
        $carFinder->registerPostEnrichmentFilter($container[$filterName]);
    }

    foreach ($enrichers as $enricherName) {
        $carFinder->registerEnrichers($container[$enricherName]);
    }

    $cars = $carFinder->find(
        $uri,
        new $parserClass
    );

    $cachedCars = $searchResultCache->fetch($sessionId);
    if ($cachedCars && is_array($cachedCars)) {
        $cars = array_merge($cars, $cachedCars);
    }

    $searchResultCache->save($sessionId, $cars, 60 * 5);

    echo " [x] Done", "\n";

    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_consume('task_queue', '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();
